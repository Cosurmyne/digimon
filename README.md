# Solution s215013395
```
Pathway II, Assignment I
ONT4101  [4206] 
Nelson Mandela University. (2020)
```
Console Application emulating characters with very specific behaviors, moving in a **two-dimensional** canvas.
![Screenshot](https://cloud.owncube.com/apps/gallery/preview.public/56524076?width=1400&height=1400&c=4068f70b803c2f54bfbefa6accd1dcc2&requesttoken=qs965siX9tD2NnnXrhrU2ZYbectwGC2cNkzY4%2Fx7rAw%3D%3Ay4IP0q%2B4wL6wVyrum1y4v8UtSIgdTH3FRSqKpJIt6Do%3D&token=7dLtPm4wPGZw9HM)  
Source available at [gitlab.com](https://gitlab.com/Cosurmyne/digimon)

The application uses the following **design patterns** for elements relevant to the described aspects;

## Design Patterns

### Strategy Pattern

* Define character motion algorithms to serve as **client** behaviors.
* Define the character base class, as the client to the strategy

### Observer Pattern

* Update the application of the x-y coordinates of the character location on canvas

### Decorator Pattern

* Decorates characters with the user selected color



In the succeeding sections we gonna delve *'under-the-hood'* to eplore what makes such a solution possible.  
Before we get carried away, let's first look at the outline of the entire application.

## UML
![Diagram](https://cloud.owncube.com/apps/gallery/preview.public/56538311?width=1400&height=1400&c=4068f70b803c2f54bfbefa6accd1dcc2&requesttoken=qs965siX9tD2NnnXrhrU2ZYbectwGC2cNkzY4%2Fx7rAw%3D%3Ay4IP0q%2B4wL6wVyrum1y4v8UtSIgdTH3FRSqKpJIt6Do%3D&token=7dLtPm4wPGZw9HM)

### Characters
* Square (Black)
* Invader (Dark Yellow)
* Random (Magenta)

The characters currently have the following **named** moves

* Tubular
* ZigZag
* Diagonal

...and can be decorated with the following colours 

* Red
* Green
* Blue

### Motion 
The application's motion is controlled by a traffic light, with the following meanings;-

* **Red** - Stop
* **Yellow** - Move Slow
* **Green** - Move Quick


## Project Scenario 
The game takes place in a 2D canvas of 45 columns by 15 lines, in a UTF-8 monospace terminal emulator window.  
Use cases of application are delivered through carefully selected **key bindings**. .   
The above outline sums up the application functionality. Let's finally jumo into code! 

### Character Base Class
If this game was a movie, the character base class would definately be the *'starring'*.  
It occurs on all three design patterns, and seems to have a very prominent role in each.  

**Client** - *Strategy Pattern*  
**Subject** - *Observer Pattern*  
**Component Base** - *Decorator Pattern*  

The insides of this class are as follows;-  

```cs
    public abstract class Character : Observable
    {
        public const char EAST = 'E';
        public const char SOUTH = 'S';
        public const char WEST = 'W';
        public const char NORTH = 'N';

        protected static byte _count = 0;

        private char _symbol;
        protected ConsoleColor skin;

        private ushort _x;
        private ushort _y;

        public ushort X
        {
            get { return _x; }
            set
            {
                _x = value;
                SetChanged();
                NotifyObservers(value);
            }
        }
        public ushort Y
        {
            get { return _y; }
            set
            {
                _y = value;
                SetChanged();
                NotifyObservers(value);
            }
        }

        public Motion Movement { get; set; }

        public ConsoleColor Skin { get { return skin; } }

        public char Direction { get; set; }

        public static byte Count { get { return _count; } }

        public Character()
        {
            this._x = (ushort)Utensil.Random(1, Config.WIDTH - 1);
            this._y = (ushort)Utensil.Random(1, Config.HEIGHT - 1);
            this._symbol = '■';
        }

        public abstract void Move();

        public bool Plot(ushort x, ushort y)
        {
            if (this.X == x && this.Y == y)
            {
                Console.ForegroundColor = this.skin;
                Console.Write(this._symbol);
                Console.ForegroundColor = ConsoleColor.Black;
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            string[] fields = GetType().ToString().Split('.');
            return fields[fields.Length - 1].PadRight(8) + ":: " + 
			this.X.ToString().PadLeft(2, '0') + "," + this.Y.ToString().PadLeft(2, '0');
        }
    }
```
This parent breeds **three** concrete classes, who's functionality spans across two of the design patterns.  
On the Strategy, these are **concrete clients**; on the Decorator, these are **concrete components**.  
These three concrete classes are **Square**, **Invader**, and **Random**.  
Fundamentally, these class are similar. Looking at one of them gives us a clear picture at all of them.  

```cs
    public class Square : Character
    {

        public Square(char direction = Character.EAST)         
		{
            base.skin = System.ConsoleColor.Black;
	    	_count++;
	    	Movement = new Tubular();
	    	Direction = direction;
        }

        public override void Move()
        {
			Movement.Move(this);
        }
    }
```
To make these individual characters move as in the reference video, we apply the Strategy pattern, defining a separate concrete strategy for each behavior.  

### The Strategy Pattern
These behaviors implement the following interface;-  

```cs
    public interface Motion
    {
        public void Move(Character target);
    }
```
An example of a **behavior algorithm**  is as follows;

```cs
    public class Tubular : Motion
    {
        private sbyte MoveRight { set; get; }
        private sbyte MoveDown { set; get; }
        private byte stretch;
        private readonly byte STRETCH_LIMIT_X = 35;
        private readonly byte STRETCH_LIMIT_Y = 4;

        public Tubular(char campus = 'E')
        {
            this.MoveRight = 1;
            this.MoveDown = 1;
            this.stretch = 0;
        }
        public void Move(Character target)
        {
            switch (target.Direction)
            {
                case Character.EAST:
                    if (target.X == Config.WIDTH - 1 || stretch == STRETCH_LIMIT_X)
                    {
                        target.Direction = Character.SOUTH;
                        stretch = 0;
                        break;
                    }
                    target.X += (ushort)(this.MoveRight = 1);
                    stretch++;
                    break;
                case Character.SOUTH:
                    if (target.Y == Config.HEIGHT || stretch == STRETCH_LIMIT_Y)
                    {
                        target.Direction = Character.WEST;
                        stretch = 0;
                        break;
                    }
                    target.Y += (ushort)(this.MoveDown = 1);
                    stretch++;
                    break;
                case Character.WEST:
                    if (target.X == 1 || stretch == STRETCH_LIMIT_X)
                    {
                        target.Direction = Character.NORTH;
                        stretch = 0;
                        break;
                    }
                    target.X += (ushort)(this.MoveRight = -1);
                    stretch++;
                    break;
                case Character.NORTH:
                    if (target.Y == 1 || stretch == STRETCH_LIMIT_Y)
                    {
                        target.Direction = Character.EAST;
                        stretch = 0;
                        break;
                    }
                    target.Y += (ushort)(this.MoveDown = -1);
                    stretch++;
                    break;
            }
        }
    }
```

s can be seen, these behaviors move the characters across the canvas in a 2D fashion.  
Each time a character moves, the display ought to be updated with the most recent location of the character, via X/Y coordinates.  

### Observer Pattern
To archieve the above, the character base needs _to be_ an Observer **Subject**.  
To be such, the character base extends the following class;

```cs
    public class Observable
    {
        private bool changed = false;
        private List<IObserver> obs;

        public Observable()
        {
            obs = new List<IObserver>();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void AddObserver(IObserver o)
        {
            if (o == null) throw new System.NullReferenceException();
            if (!obs.Contains(o)) obs.Add(o);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void RemoveObserver(IObserver o)
        {
            obs.Remove(o);
        }

        public void NotifyObservers()
        {
            NotifyObservers(null);
        }

        public void NotifyObservers(object arg)
        {
            object[] arrLocal;

            lock (this)
            {
                if (!changed) return;
                arrLocal = obs.ToArray();
                ClearChanged();
            }
            for (int i = 0; i < arrLocal.Length; i++) ((IObserver)arrLocal[i]).Update(this, arg);

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void RemoveObservers()
        {
            obs.Clear();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        protected void SetChanged()
        {
            changed = true;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        protected void ClearChanged()
        {
            changed = false;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public bool HasChanged()
        {
            return changed;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public int CountObservers()
        {
            return obs.Count;
        }
    }
```
Using **composition** and **polymophism**, this class pushes coordinates of the latest location of its child *(in this case the character base)* through the following observer;

```cs
    public class ObserveCharacter : IObserver
    {
	    private ushort x;
	    private ushort y;
	    private Observable observable;

        public ObserveCharacter(Observable observable)
        {
            this.observable = observable;
	    observable.AddObserver((IObserver)this);
        }

        public void Update(Observable obs, object arg)
        {
		if(obs is Character)
		{
			Character player = (Character) obs;
			this.x = player.X;
			this.y = player.Y;
		}
        }
    }
```

Part of the character's visible features is to be able to **decorate** them with the three basic clours.  

### The Decorator Pattern
This is made possible by the following decorator base, which extends directly from the character base;

```cs
    public abstract class Decorator : Character
    {
        protected Character component;

        public Decorator(Character component)
        {
            this.component = component;
        }

        public override void Move()
        {
		if (component != null)
		{
			component.Move();
		}
        }
    }
```

This base breeds the three base colours, that visually wrap a new character spawn.

```cs
namespace root
{
    public class Red : Decorator
    {
        public Red(Character component) : base(component)
        {
        }

        public override void Move()
        {
            base.Move();
            Convoy();
        }
        void Convoy()
        {
			// code wraping the character (convoy chars of the specified colour)
        }
    }
}
```

Ultimately, when executing a program, the compiler looks for only one Method;

```cs
﻿using System;
using System.Threading;
using System.Collections.Generic;

namespace root
{
    class Program
    {
        static bool exit = false;
        static bool motion = true;
        static List<char> keystrokes = new List<char>();

        static void Main(string[] args)
        {
			Console.Title = "Digimon";
            Thread pace = new Thread(TrafficLights);
            pace.Start();

            List<Character> p = new List<Character>();
            Display(p, pace);
        }
        private static void Display(List<Character> p, Thread pace)
        {
			// code...
        }
        private static void Decorating(ConsoleKey role)
        {
			// code...
        }

        private static void Canvas(List<Character> p)
        {
            // code...
        }

        private static void TrafficLights()
        {
            Config ctrl = Config.GetConfig();
            System.Random rand = new System.Random();
            while (!exit)
            {
                switch (rand.Next(0, 3))
                {
                    case 0:
                        motion = false;
                        ctrl.Motion(false); // normal speed
                        break;
                    case 1:
                        motion = true;
                        ctrl.Motion(false); // normal speed
                        break;
                    case 2:
                        motion = true;
                        ctrl.Motion(); // fast foward
                        break;
                }
                Thread.Sleep(3500);
            }
        }
    }
}
```
*...rest of the code is at [https://gitlab.com/Cosurmyne/digimon/-/blob/master/src/Program.cs](https://gitlab.com/Cosurmyne/digimon/-/blob/master/src/Program.cs)*

## Conclusion
I had the most fun coding this exercise, and was quite an awe to witness these patterns all come together organicly.  
The development of this solution doesn't end with the assessment deadline, but continuess to see the realization of the missing functions.  

A port to other languages, e.g. **c++** and **php**, is also contemplated, and not farefetched, nor scratched out.  
Feto el free to be an active member, and contibute to the betterment of app, via the offial [repo](https://gitlab.com/Cosurmyne/digimon) 