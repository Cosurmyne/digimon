using System;

namespace root
{
    static class Utensil
    {
        private static System.Random rand = new System.Random();

        public static void Title(string text, string prefix = "")
        {
            Console.WriteLine();
            Console.WriteLine("{1}{0}", text, prefix);
            Console.Write(prefix);
            for (byte i = 0; i < text.Length; i++) Console.Write('=');
            Console.WriteLine();
        }

        public static int Random(int min, int max)
        {
            return rand.Next(min, max);
        }
        public static void ControlPanel(bool motion)
        {
            Config ctrl = Config.GetConfig();
            Title("Heads-Up Display, Action Key Bindings & Control", " ");

            Console.Write(' ');
            Console.ForegroundColor = !motion ? ConsoleColor.Red : ConsoleColor.Gray;
            Console.Write('■');
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Write(" ");
            Console.Write("┊ use key '");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("{0}", Config.KEY_SQR);
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Write("' to add character 'Square'");
            Console.WriteLine();

            Console.Write(' ');
            Console.ForegroundColor = motion && ctrl.Speed == Config.NORMAL ? ConsoleColor.DarkYellow : ConsoleColor.Gray;
            Console.Write('■');
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Write(" ");
            Console.Write("┊ use key '");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("{0}", Config.KEY_INV);
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Write("' to add character 'Invader'");
            Console.WriteLine();

            Console.Write(' ');
            Console.ForegroundColor = motion && ctrl.Speed == Config.RUNNING ? ConsoleColor.DarkGreen : ConsoleColor.Gray;
            Console.Write('■');
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Write(" ");
            Console.Write("┊ use key '");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("{0}", Config.KEY_RND);
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Write("' to add character 'Random'");
            Console.WriteLine();

            Console.WriteLine("------------------------------------------------");
            const byte pad = 43;
            Console.Write(" {0}>> ", "Key to decorate character with Red".PadRight(pad));
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("{0}", Config.KEY_RED);
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine();
            Console.Write(" {0}>> ", "Key to decorate character with Green".PadRight(pad));
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("{0}", Config.KEY_GREEN);
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine();
            Console.Write(" {0}>> ", "Key to decorate character with Blue".PadRight(pad));
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("{0}", Config.KEY_BLUE);
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine();
            Console.Write(" {0}>> ", "Key to Terminate Program Execution".PadRight(pad));
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("{0}", Config.KEY_QUIT);
            Console.ForegroundColor = ConsoleColor.Black;
            Console.WriteLine();
        }
    }
}
