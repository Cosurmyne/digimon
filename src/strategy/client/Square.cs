namespace root
{
    public class Square : Character
    {

        public Square(char direction = Character.EAST)         {
            base.skin = System.ConsoleColor.Black;
	    _count++;
	    Movement = new Tubular();
	    Direction = direction;
        }

        public override void Move()
        {
		Movement.Move(this);
        }
    }
}
