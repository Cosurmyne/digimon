using System;

namespace root
{
    public abstract class Character : Observable
    {
        public const char EAST = 'E';
        public const char SOUTH = 'S';
        public const char WEST = 'W';
        public const char NORTH = 'N';

        protected static byte _count = 0;

        private char _symbol;
        protected ConsoleColor skin;

        private ushort _x;
        private ushort _y;

        public ushort X
        {
            get { return _x; }
            set
            {
                _x = value;
                SetChanged();
                NotifyObservers(value);
            }
        }
        public ushort Y
        {
            get { return _y; }
            set
            {
                _y = value;
                SetChanged();
                NotifyObservers(value);
            }
        }

        public Motion Movement { get; set; }

        public ConsoleColor Skin { get { return skin; } }

        public char Direction { get; set; }

        public static byte Count { get { return _count; } }

        public Character()
        {
            this._x = (ushort)Utensil.Random(1, Config.WIDTH - 1);
            this._y = (ushort)Utensil.Random(1, Config.HEIGHT - 1);
            this._symbol = '■';
        }

        public abstract void Move();

        public bool Plot(ushort x, ushort y)
        {
            if (this.X == x && this.Y == y)
            {
                Console.ForegroundColor = this.skin;
                Console.Write(this._symbol);
                Console.ForegroundColor = ConsoleColor.Black;
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            string[] fields = GetType().ToString().Split('.');
            return fields[fields.Length - 1].PadRight(8) + ":: " + this.X.ToString().PadLeft(2, '0') + "," + this.Y.ToString().PadLeft(2, '0');
        }
    }
}
