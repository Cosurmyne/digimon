namespace root
{
    public class Invader : Character
    {

        public Invader(char direction = Character.EAST)         {
            base.skin = System.ConsoleColor.DarkYellow;
	    _count++;
	    Movement = new ZigZag();
	    Direction = direction;
        }

        public override void Move()
        {
		Movement.Move(this);
        }
    }
}
