namespace root
{
    public class Random : Character
    {

        public Random(char direction = Character.EAST)         {
            base.skin = System.ConsoleColor.Magenta;
	    _count++;
	    Movement = new Diagonal();
        }

        public override void Move()
        {
		Movement.Move(this);
        }
    }
}
