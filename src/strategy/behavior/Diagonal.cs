namespace root
{
    public class Diagonal : Motion
    {
        protected sbyte MoveRight { set; get; }
        protected sbyte MoveDown { set; get; }
        public Diagonal()
        {
            this.MoveRight = 1;
            this.MoveDown = 1;
        }
        public void Move(Character target)
        {
            Config dimensions = Config.GetConfig();
            if (target.X == 1 || target.X == Config.WIDTH) this.MoveRight *= -1;
            if (target.Y == 1 || target.Y == Config.HEIGHT) this.MoveDown *= -1;
            target.X += (ushort)this.MoveRight;
            target.Y += (ushort)this.MoveDown;
        }
    }
}
