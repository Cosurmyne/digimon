namespace root
{
    public class ZigZag : Motion
    {
        private sbyte MoveRight { set; get; }
        private sbyte MoveDown { set; get; }
        private byte stretch;
        private bool down;
        private readonly byte STRETCH_LIMIT_X = 35;
        private readonly byte STRETCH_LIMIT_Y = 2;

        public ZigZag()
        {
            this.MoveRight = 1;
            this.MoveDown = 1;
            this.stretch = 0;
            this.down = true;
        }
        public void Move(Character target)
        {
            switch (target.Direction)
            {
                case Character.EAST:
                    if (target.X == Config.WIDTH - 1 || stretch == STRETCH_LIMIT_X)
                    {
                        target.Direction = this.down ? Character.SOUTH : Character.NORTH;
                        stretch = 0;
                        break;
                    }
                    target.X += (ushort)(this.MoveRight = 1);
                    stretch++;
                    break;

                case Character.SOUTH:
                    if (target.Y == Config.HEIGHT)
                    {
                        target.Direction = XSwitch(target.X);
                        this.down = false;
                        stretch = 0;
                        break;
                    }
                    if (stretch == STRETCH_LIMIT_Y)
                    {
                        target.Direction = XSwitch(target.X);
                        stretch = 0;
                        break;
                    }
                    target.Y += (ushort)(this.MoveDown = 1);
                    stretch++;
                    break;

                case Character.WEST:
                    if (target.X == 1 || stretch == STRETCH_LIMIT_X)
                    {
                        target.Direction = this.down ? Character.SOUTH : Character.NORTH;
                        stretch = 0;
                        break;
                    }
                    target.X += (ushort)(this.MoveRight = -1);
                    stretch++;
                    break;

                case Character.NORTH:
                    if (target.Y == 1)
                    {
                        target.Direction = XSwitch(target.X);
                        this.down = true;
                        stretch = 0;
                        break;
                    }
                    if (stretch == STRETCH_LIMIT_Y)
                    {
                        target.Direction = XSwitch(target.X);
                        stretch = 0;
                        break;
                    }
                    target.Y += (ushort)(this.MoveDown = -1);
                    stretch++;
                    break;
            }
        }
        private char XSwitch(ushort x)
        {
            byte split = (byte)decimal.Round((decimal)(Config.WIDTH / 2.0));
            if (x > split) return Character.WEST;
            return Character.EAST;
        }
    }
}
