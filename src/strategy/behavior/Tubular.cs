namespace root
{
    public class Tubular : Motion
    {
        private sbyte MoveRight { set; get; }
        private sbyte MoveDown { set; get; }
        private byte stretch;
        private readonly byte STRETCH_LIMIT_X = 35;
        private readonly byte STRETCH_LIMIT_Y = 4;

        public Tubular(char campus = 'E')
        {
            this.MoveRight = 1;
            this.MoveDown = 1;
            this.stretch = 0;
        }
        public void Move(Character target)
        {
            switch (target.Direction)
            {
                case Character.EAST:
                    if (target.X == Config.WIDTH - 1 || stretch == STRETCH_LIMIT_X)
                    {
                        target.Direction = Character.SOUTH;
                        stretch = 0;
                        break;
                    }
                    target.X += (ushort)(this.MoveRight = 1);
                    stretch++;
                    break;
                case Character.SOUTH:
                    if (target.Y == Config.HEIGHT || stretch == STRETCH_LIMIT_Y)
                    {
                        target.Direction = Character.WEST;
                        stretch = 0;
                        break;
                    }
                    target.Y += (ushort)(this.MoveDown = 1);
                    stretch++;
                    break;
                case Character.WEST:
                    if (target.X == 1 || stretch == STRETCH_LIMIT_X)
                    {
                        target.Direction = Character.NORTH;
                        stretch = 0;
                        break;
                    }
                    target.X += (ushort)(this.MoveRight = -1);
                    stretch++;
                    break;
                case Character.NORTH:
                    if (target.Y == 1 || stretch == STRETCH_LIMIT_Y)
                    {
                        target.Direction = Character.EAST;
                        stretch = 0;
                        break;
                    }
                    target.Y += (ushort)(this.MoveDown = -1);
                    stretch++;
                    break;
            }
        }
    }
}
