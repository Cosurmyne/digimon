namespace root
{
    public interface Motion
    {
        public void Move(Character target);
    }
}
