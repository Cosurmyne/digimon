namespace root
{
    public sealed class Config
    {
        public const byte NORMAL = 128;
        public const byte RUNNING = 48;
        public const ushort WIDTH = 45;
        public const ushort HEIGHT = 15;
	
	public const System.ConsoleKey KEY_QUIT = System.ConsoleKey.Q;
	
	public const System.ConsoleKey KEY_RED = System.ConsoleKey.R;
	public const System.ConsoleKey KEY_GREEN = System.ConsoleKey.G;
	public const System.ConsoleKey KEY_BLUE = System.ConsoleKey.B;
	
	public const System.ConsoleKey KEY_SQR = System.ConsoleKey.S;
	public const System.ConsoleKey KEY_INV = System.ConsoleKey.I;
	public const System.ConsoleKey KEY_RND = System.ConsoleKey.N;

        private byte speed;

        private static Config _instance;
        private static readonly object _lockThis = new object();
        private Config()
        {
            this.Motion(false); // normslize speed
        }

        public byte Speed { get { return speed; } }


        public static Config GetConfig()
        {
            lock (_lockThis)
            {
                if (_instance == null) _instance = new Config();
            }
            return _instance;
        }

        public void Motion(bool boost = true)
        {
            if (boost) this.speed = RUNNING;
            else this.speed = NORMAL;
        }
    }
}
