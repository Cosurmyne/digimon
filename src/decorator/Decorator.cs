namespace root
{
    public abstract class Decorator : Character
    {
        protected Character component;

        public Decorator(Character component)
        {
            this.component = component;
        }

        public override void Move()
        {
		if (component != null)
		{
			component.Move();
		}
        }
    }
}
