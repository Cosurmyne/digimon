﻿using System;
using System.Threading;
using System.Collections.Generic;

namespace root
{
    class Program
    {
        static bool exit = false;
        static bool motion = true;
        static List<char> keystrokes = new List<char>();

        static void Main(string[] args)
        {
		Console.Title = "Digimon";
            Thread pace = new Thread(TrafficLights);
            pace.Start();

            List<Character> p = new List<Character>();
            Display(p, pace);
        }
        private static void Display(List<Character> p, Thread pace)
        {
            do
            {
                Config ctrl = Config.GetConfig();
                do
                {
                    Console.Clear();
                    Canvas(p);

                    Utensil.ControlPanel(motion);

                    if (Character.Count > 0) Utensil.Title("Characters :: " + Character.Count.ToString().PadLeft(2, '0'), " ");
                    const byte LINE_LIMIT = 4;
                    for (ushort k = 0; k < p.Count; k++)
                    {
                        if (p.Count <= LINE_LIMIT || (k >= p.Count - LINE_LIMIT))
                        {
                            Console.ForegroundColor = p[k].Skin;
                            Console.WriteLine(" {0}", p[k].ToString());
                            Console.ForegroundColor = ConsoleColor.Black;
                        }
                        if (motion) p[(int)k].Move();
                    }
                    Thread.Sleep(ctrl.Speed);
                }
                while (!Console.KeyAvailable);

                switch (Console.ReadKey(true).Key)
                {
                    case Config.KEY_RED:
                        keystrokes.Add('R');
                        break;
                    case Config.KEY_GREEN:
                        keystrokes.Add('G');
                        break;
                    case Config.KEY_BLUE:
                        keystrokes.Add('B');
                        break;
                    case Config.KEY_SQR:
                        if (keystrokes.Count > 0)
                        {
                            Decorating(Config.KEY_SQR);
                            break;
                        }
                        p.Add(new Square());
                        break;
                    case Config.KEY_INV:
                        if (keystrokes.Count > 0)
                        {
                            Decorating(Config.KEY_INV);
                            break;
                        }
                        p.Add(new Invader());
                        break;
                    case Config.KEY_RND:
                        if (keystrokes.Count > 0)
                        {
                            Decorating(Config.KEY_RND);
                            break;
                        }
                        p.Add(new Random());
                        break;
                    case Config.KEY_QUIT:
                        exit = true;
                        pace.Interrupt();
                        break;
                }

            } while (!exit);
            Console.Clear();
        }
        private static void Decorating(ConsoleKey role)
        {
		Character avatar = null;
		Decorator target = null;
		List<Decorator> ingredients = new List<Decorator>();
		switch(role)
		{
			case Config.KEY_SQR: avatar = new Square(); break;
			case Config.KEY_INV: avatar = new Invader(); break;
			case Config.KEY_RND: avatar = new Random(); break;
		}
            foreach (char key in keystrokes)
            {
                switch (key)
                {
                    case 'R':target = new Red(avatar);break;
                    case 'G':target = new Green(avatar);break;
                    case 'B':target = new Blue(avatar);break;
                }
		ingredients.Add(target);
            }
            keystrokes.Clear();
        }

        private static void Canvas(List<Character> p)
        {
            Config dimensions = Config.GetConfig();
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;

            Console.Write(' ');
            Console.Write('+');
            for (byte i = 0; i < Config.WIDTH; i++) Console.Write('-');
            Console.Write('+');
            Console.WriteLine();
            for (ushort y = 1; y <= Config.HEIGHT; y++)
            {
                Console.Write(' ');
                Console.Write('┆');
                for (ushort x = 1; x <= Config.WIDTH; x++)
                {
                    bool nothing = true;
                    for (uint k = 0; k < p.Count; k++) if (p[(int)k].Plot(x, y)) nothing = false;
                    if (nothing) Console.Write(' ');
                }
                Console.Write('┆');
                Console.WriteLine();
            }
            Console.Write(' ');
            Console.Write('+');
            for (byte i = 0; i < Config.WIDTH; i++) Console.Write('-');
            Console.Write('+');
        }

        private static void TrafficLights()
        {
            Config ctrl = Config.GetConfig();
            System.Random rand = new System.Random();
            while (!exit)
            {
                switch (rand.Next(0, 3))
                {
                    case 0:
                        motion = false;
                        ctrl.Motion(false); // normal speed
                        break;
                    case 1:
                        motion = true;
                        ctrl.Motion(false); // normal speed
                        break;
                    case 2:
                        motion = true;
                        ctrl.Motion(); // fast foward
                        break;
                }
                Thread.Sleep(3500);
            }
        }
    }
}
