namespace root
{
    public class ObserveCharacter : IObserver
    {
	    private ushort x;
	    private ushort y;
	    private Observable observable;

        public ObserveCharacter(Observable observable)
        {
            this.observable = observable;
	    observable.AddObserver((IObserver)this);
        }

        public void Update(Observable obs, object arg)
        {
		if(obs is Character)
		{
			Character player = (Character) obs;
			this.x = player.X;
			this.y = player.Y;
		}
        }
    }
}
