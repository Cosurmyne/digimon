using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace root
{
    public class Observable
    {
        private bool changed = false;
        private List<IObserver> obs;

        public Observable()
        {
            obs = new List<IObserver>();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void AddObserver(IObserver o)
        {
            if (o == null) throw new System.NullReferenceException();
            if (!obs.Contains(o)) obs.Add(o);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void RemoveObserver(IObserver o)
        {
            obs.Remove(o);
        }

        public void NotifyObservers()
        {
            NotifyObservers(null);
        }

        public void NotifyObservers(object arg)
        {
            object[] arrLocal;

            lock (this)
            {
                if (!changed) return;
                arrLocal = obs.ToArray();
                ClearChanged();
            }
            for (int i = 0; i < arrLocal.Length; i++) ((IObserver)arrLocal[i]).Update(this, arg);
            //for (int i = arrLocal.Length - 1; i >= 0; i--) ((IObserver)arrLocal[i]).Update(this, arg);
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void RemoveObservers()
        {
            obs.Clear();
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        protected void SetChanged()
        {
            changed = true;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        protected void ClearChanged()
        {
            changed = false;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public bool HasChanged()
        {
            return changed;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public int CountObservers()
        {
            return obs.Count;
        }
    }
}
