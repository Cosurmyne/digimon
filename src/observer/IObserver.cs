//Ĥ
namespace root
{
    public interface IObserver
    {
        void Update(Observable o, object arg);
    }
}
